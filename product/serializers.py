from rest_framework import serializers

from core.models import Product, Image

from image import serializers as ImageSerializer
from favorite_product import serializers as FavoriteProductSerializer


class ProductSerializer(serializers.ModelSerializer):
    """Serializer for product objects"""
    images = ImageSerializer.ImageSerializer(many=True, read_only=True)
    favorite_products = FavoriteProductSerializer.FavoriteProductSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        fields = [field.name for field in model._meta.fields]
        fields.append('images')
        fields.append('favorite_products')
        read_only_fields = ('id',)

    def create(self, validated_data):
        images = self.context.get('view').request.FILES
        product = Product.objects.create(**validated_data)

        for image in images.values():
            Image.objects.create(product=product, file=image)
        return product
