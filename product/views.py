from rest_framework import viewsets

from core.models import Product

from product import serializers


# def _params_to_ints(qs):
#     """Convert a list of string IDs to a list of integers"""
#     return [int(str_id) for str_id in qs.split(',')]


class ProductViewSet(viewsets.ModelViewSet):
    """Manage products in the database"""
    serializer_class = serializers.ProductSerializer
    queryset = Product.objects.all()

    # def get_queryset(self):
    #     """Filter products"""
    #     category = self.request.query_params.get('category')
    #     if category:
    #         category_id = _params_to_ints(category)
    #         self.queryset = self.queryset.filter(category__id__in=category_id)
    #
    #     return self.queryset
