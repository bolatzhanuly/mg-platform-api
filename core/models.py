from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, \
    PermissionsMixin
from django.conf import settings


class UserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        """Creates and saves new user"""
        if not email:
            raise ValueError('Введите электронную почту')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """Creates and saves a new super user"""
        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that supports using email instead of username"""
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255, default='Фамилия')
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    company = models.ForeignKey('Company', null=True, default=None, on_delete=models.SET_NULL)

    objects = UserManager()

    USERNAME_FIELD = 'email'


class Company(models.Model):
    """To sell products in our platform user must create company"""
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    """Product object"""
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    price = models.IntegerField(default=-1)
    link = models.CharField(max_length=255, blank=True)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    group = models.ForeignKey('Group', null=True, default=None, on_delete=models.SET_NULL)
    category = models.ForeignKey('Category', null=True, default=None, on_delete=models.SET_NULL)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        default=None,
        null=True
    )
    favorite_product = models.ForeignKey(
        'FavoriteProduct',
        related_name='favorite_products',
        on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return self.title


class Image(models.Model):
    """Image of product"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    file = models.FileField(blank=False, null=False)
    product = models.ForeignKey('Product', related_name='images', on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Фотография"
        verbose_name_plural = "Фотографии"


class Group(models.Model):
    """My company groups of products"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    file = models.FileField(blank=False, null=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    """Categories of products in platform"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class FavoriteProduct(models.Model):
    """Favorite product object"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    product = models.ForeignKey(
        'Product',
        related_name='favorite_products',
        on_delete=models.CASCADE, null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )


class CartProduct(models.Model):
    """Cart product object"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    quantity = models.PositiveIntegerField(default=1)
    product = models.ForeignKey(
        'Product',
        related_name='cart_products',
        on_delete=models.CASCADE, null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )


class Order(models.Model):
    """Order object"""
    created = models.DateTimeField(auto_now_add=True, null=True)
    clientName = models.CharField(max_length=255)
    clientSurname = models.CharField(max_length=255)
    clientPhone = models.CharField(max_length=255)
    clientEmail = models.CharField(max_length=255)
    nameOfOrganization = models.CharField(max_length=255, null=True)
    binOfOrganization = models.CharField(max_length=255, null=True)
    addressOfOrganization = models.CharField(max_length=255, null=True)
    iikOfOrganization = models.CharField(max_length=255, null=True)
    bikOfOrganization = models.CharField(max_length=255, null=True)
    addressOfOrder = models.CharField(max_length=255, null=True)
    dateOfOrder = models.CharField(max_length=255, null=True)
    commentOfOrder = models.CharField(max_length=255, null=True)
    products = models.CharField(max_length=255)
    productsQuantity = models.CharField(max_length=255)
    priceOfOrder = models.CharField(max_length=255)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
