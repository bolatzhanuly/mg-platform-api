# Generated by Django 3.0.6 on 2020-06-18 09:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20200618_1232'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='link',
        ),
    ]
