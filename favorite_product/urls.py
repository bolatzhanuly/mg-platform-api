from django.urls import path, include
from rest_framework.routers import DefaultRouter

from favorite_product import views


router = DefaultRouter()
router.register('', views.FavoriteProductViewSet)

app_name = 'favorite product'

urlpatterns = [
    path('', include(router.urls))
]
