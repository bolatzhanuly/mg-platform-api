from rest_framework import serializers

from core.models import FavoriteProduct


class FavoriteProductSerializer(serializers.ModelSerializer):
    """Serializer for favorite products objects"""

    class Meta:
        model = FavoriteProduct
        fields = ('__all__')
        read_only_fields = ('id',)
