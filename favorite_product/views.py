from rest_framework import viewsets

from core.models import FavoriteProduct

from favorite_product import serializers


class FavoriteProductViewSet(viewsets.ModelViewSet):
    """Manage favorite products in the database"""
    serializer_class = serializers.FavoriteProductSerializer
    queryset = FavoriteProduct.objects.all()
