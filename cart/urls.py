from django.urls import path, include
from rest_framework.routers import DefaultRouter

from cart import views


router = DefaultRouter()
router.register('', views.CartProductViewSet)

app_name = 'cart product'

urlpatterns = [
    path('', include(router.urls))
]
