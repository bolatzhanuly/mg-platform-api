from rest_framework import viewsets

from core.models import CartProduct

from cart import serializers


class CartProductViewSet(viewsets.ModelViewSet):
    """Manage cart products in the database"""
    serializer_class = serializers.CartProductSerializer
    queryset = CartProduct.objects.all()
