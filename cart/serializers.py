from rest_framework import serializers

from core.models import CartProduct


class CartProductSerializer(serializers.ModelSerializer):
    """Serializer for cart products objects"""

    class Meta:
        model = CartProduct
        fields = ('__all__')
        read_only_fields = ('id',)