from rest_framework import viewsets

from core.models import Order

from order import serializers


class OrderViewSet(viewsets.ModelViewSet):
    """Manage orders in the database"""
    serializer_class = serializers.OrderSerializer
    queryset = Order.objects.all()
