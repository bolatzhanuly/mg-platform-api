from rest_framework import serializers

from core.models import Order


class OrderSerializer(serializers.ModelSerializer):
    """Serializer for order objects"""

    class Meta:
        model = Order
        fields = ('__all__')
        read_only_fields = ('id',)