from rest_framework import viewsets

from core.models import Category

from category import serializers


class CategoryViewSet(viewsets.ModelViewSet):
    """Manage categories in the database"""
    serializer_class = serializers.CategorySerializer
    queryset = Category.objects.all()
