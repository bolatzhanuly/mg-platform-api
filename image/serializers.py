from rest_framework import serializers

from core.models import Image


class ImageSerializer(serializers.ModelSerializer):
    """Serializer for image objects"""

    class Meta:
        model = Image
        fields = '__all__'
