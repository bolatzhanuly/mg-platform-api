from rest_framework import viewsets

from image import serializers

from core import models


class ImageViewSet(viewsets.ModelViewSet):
    """Manage images in the database"""
    queryset = models.Image.objects.all()
    serializer_class = serializers.ImageSerializer

