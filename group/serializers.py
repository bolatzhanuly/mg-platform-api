from rest_framework import serializers

from core.models import Group


class GroupSerializer(serializers.ModelSerializer):
    """Serializer for group objects"""

    class Meta:
        model = Group
        fields = ('__all__')
        read_only_fields = ('id',)
