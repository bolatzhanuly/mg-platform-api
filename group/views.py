from rest_framework import viewsets

from core.models import Group

from group import serializers


class GroupViewSet(viewsets.ModelViewSet):
    """Manage groups in the database"""
    serializer_class = serializers.GroupSerializer
    queryset = Group.objects.all()
